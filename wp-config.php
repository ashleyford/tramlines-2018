<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/home/tramlines/public_html/2018/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'tramline_wordpress');

/** MySQL database username */
define('DB_USER', 'tramline_wp');

/** MySQL database password */
define('DB_PASSWORD', 'Pe-G6_v=L]Ka');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']@| |n[Nz>uzOQne8}diq6[gzq^r%muR`&<vXgp9?.d&FJjDJyWtQ|h9}0udwR)h');
define('SECURE_AUTH_KEY',  '-3l*A y7L;lkV<40v.[i-L(+zZR[XY:a5*-^Z+Yg!6$]_Q_B)Hk6I$&fKZq|7%;-');
define('LOGGED_IN_KEY',    '%M3%EpBP7,Bn](?9&pn]7,`r/:XZs3MMc0JbDUJqL6UW[Z7cSuvC~XM(HLASIf54');
define('NONCE_KEY',        'cft+:Mgl2^26(DH2zD>&@|3{+}s:7y7m=,/rqv`VC!oNAqb#h+q%l@;$TPlR;ez~');
define('AUTH_SALT',        'zMsEle{Mdr,/${I^Fv- dyciu<&r`?+{V( 3oxf+A$$5` Pc *c1TYK{V0fD>}3V');
define('SECURE_AUTH_SALT', 'l4^pPPMuPR;G9B~`$jM,FO~E&spP8<1c&QLYw7Np&]=&W8kXbH[{/Tg3wJEJtaSl');
define('LOGGED_IN_SALT',   ')5sdnNm.+=7PGe-]Wu/Gpyi+z?^5HXiK%ph7ms=9*KmF>}<gX^xho*s{#:9hp~[1');
define('NONCE_SALT',       't+bVhkk~beix&{8cfz^H:lW3jEKnMn. be[|x6wf<.p|Vd?%%SY :`copv#vf%!2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define( 'WP_MEMORY_LIMIT', '256M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
