// Define the root namespace if necessary
var CCWP = window.CCWP || {};


/**
* Behaviours for the charting elements
* @namespace
*/
CCWP.formValidation = {

      config: {
        formClass: '.custom-form',
        formElementClass: '.form-element',
        formCheckboxClass: '.form-checkbox',
        formTextareaClass: '.form-textarea',
        inputErrorClass: 'form-input-error',
        errorMessages: {
          valid_number : 'Please enter a valid number.',
          valid_url : 'Please enter a valid URL.',
          valid_email : 'Please enter a valid email.',
          required: 'This field is required.'
        }
      },

      init: function() {

         that = this;

         $(that.config.formElementClass).focus(function() {
           $(this).removeClass('form-input-error');
           $(this).parent().find('.form-error-message').hide();
         });

         $(that.config.formElementClass).focusout(function() {
            if($(this).data("validate")){
              validate_key = $(this).data("validate");
              // call the correct validate function
              var validate_response =  window["CCWP"]["formValidation"][validate_key](this);

              if(validate_response === false){
                that.error_handler(this, that.config.errorMessages[validate_key]);
              }

            }
         });

         $(".form-submit").click(function(e){
            e.preventDefault();
            that.submit_btn();
         });

      },

      required: function(element) {

        if(element.value == '' || element.value === null){
          return false;
        }
        return true;

      },

      valid_email: function(element) {

        var email = element.value;
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email.toLowerCase());

      },

      valid_url: function(element) {

        var url = element.value;

        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
         '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
         '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
         '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
         '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
         '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
         return pattern.test(url);

      },

      submit_btn: function() {

        var has_errors = false;

        $('.form-element').each(function(){
          if($(this).data("validate")){
            validate_key = $(this).data("validate");
              // call the correct function
            var validate_response =  window.CCWP.formValidation[validate_key](this);
            if(validate_response === false){
              that.error_handler(this, that.config.errorMessages[validate_key]);
              has_errors = true;
            }
          }
        });

        if (has_errors === false ){
          $(".custom-form").submit();
        }

      },

      error_handler: function(element, errorMessage) {
        $(element).addClass('form-input-error');
        $(element).parent().find('.form-error-message').html(errorMessage).show();
      }

};
