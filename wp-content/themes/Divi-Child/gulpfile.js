var gulp = require('gulp'),
  sass = require('gulp-sass'),
  rename = require('gulp-rename'),
  cssmin = require('gulp-minify-css'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  jshint = require('gulp-jshint'),
  scsslint = require('gulp-sass-lint'),
  cache = require('gulp-cached'),
  prefix = require('gulp-autoprefixer'),
  browserSync = require('browser-sync'),
  reload = browserSync.reload,
  minifyHTML = require('gulp-minify-html'),
  size = require('gulp-size'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  plumber = require('gulp-plumber'),
  deploy = require('gulp-gh-pages'),
  notify = require('gulp-notify'),
  mainBowerFiles = require('gulp-main-bower-files'),
  gulpFilter = require('gulp-filter');


gulp.task('bower', function() {
  var filterJS = gulpFilter('**/*.js', {
    restore: true
  });
  return gulp.src('bower.json')
    .pipe(mainBowerFiles({
      // overrides: {
      //   hoverintent: {
      //       main: ['jquery.hoverintent.js'],
      //       dependencies: {}
      //   }
      //  }
    }))
    .pipe(filterJS)
    .pipe(concat('vendor.min.js'))
    .pipe(uglify())
    .pipe(filterJS.restore)
    .pipe(gulp.dest(''));
});

gulp.task('scss', function() {
  var onError = function(err) {
    notify.onError({
      title: "Gulp",
      subtitle: "Failure!",
      message: "Error: <%= error.message %>",
      sound: "Beep"
    })(err);
    this.emit('end');
  };


  return gulp.src('dev/scss/style.scss')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sass())
    .pipe(size({
      gzip: true,
      showFiles: true
    }))
    .pipe(prefix())
    .pipe(rename('style.css'))
    .pipe(gulp.dest(''))
    .pipe(reload({
      stream: true
    }))
    .pipe(cssmin())
    .pipe(size({
      gzip: true,
      showFiles: true
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(''))
});

gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: "dev/"
    }
  });
});

// gulp.task('deploy', function () {
//     return gulp.src('dist/**/*')
//         .pipe(deploy());
// });

gulp.task('js', function() {
  gulp.src('dev/js/*.js')
    .pipe(uglify())
    .pipe(size({
      gzip: true,
      showFiles: true
    }))
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest(''))
    .pipe(reload({
      stream: true
    }));
});

gulp.task('scss-lint', function() {
  gulp.src('dev/scss/**/*.scss')
    .pipe(cache('scsslint'))
    .pipe(scsslint());
});

gulp.task('jshint', function() {
  gulp.src('dev/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('watch', function() {
  gulp.watch('dev/scss/**/*.scss', ['scss']);
  gulp.watch('dev/js/*.js', ['jshint', 'js']);
  gulp.watch('dev/img/*', ['imgmin']);
});

gulp.task('imgmin', function() {
  return gulp.src('dev/img/*')
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{
        removeViewBox: false
      }],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('img'));
});

gulp.task('default', ['js', 'imgmin', 'bower', 'scss', 'watch']);
