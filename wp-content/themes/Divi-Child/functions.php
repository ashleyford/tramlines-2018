<?php

// remove emoji code from WP
require_once 'Config/remove_emoji_scripts.php';
// enqueue styles css and js
require_once 'Config/enqueue_styles.php';
// add svg support in the media library
require_once 'Config/support_svgs.php';
// load in ac
//define( 'ACF_LITE', true ); // this hides the AC plugin in the admin dashboard
// require_once 'Config/acf_config.php';
// require_once 'Config/acf.php';
// html compression
//require_once 'Config/compress_html.php';
