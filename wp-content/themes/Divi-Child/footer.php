<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">

				<div class="container">
					<h3>Sign Up To Our Newsletter</h3>
				<form class="mailing-list custom-form clearfix" method="POST" action="https://broadwicklive.us3.list-manage.com/subscribe/post?u=aa8f30e79ad68283f547ec9e7&amp;id=26d9483b55" name="mc-embedded-subscribe-form" novalidate>
					<div class="custom-form__group">
						<input class="custom-form__input form-element" data-validate="valid_email" type="email" name="EMAIL" placeholder="Email address" required>
						<div class="form-error-message"></div>
					</div>
					<div class="custom-form__group">
						<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_aa8f30e79ad68283f547ec9e7_26d9483b55" tabindex="-1" value=""></div>
						<input class="custom-form__submit form-submit " type="submit" name="subscribe" value="Sign Up">
					</div>
				</form>
				</div>

				<div id="footer-bottom">
					<div class="container clearfix">
						<h3>Follow Us</h3>
						<?php
							if ( false !== et_get_option( 'show_footer_social_icons', true ) ) { ?>


								<ul class="et-social-icons">

								<?php if ( 'on' === et_get_option( 'divi_show_facebook_icon', 'on' ) ) : ?>
									<li class="et-social-icon et-social-facebook">
										<a href="<?php echo esc_url( et_get_option( 'divi_facebook_url', '#' ) ); ?>" class="icon">
											<span><?php esc_html_e( 'Facebook', 'Divi' ); ?></span>
										</a>
									</li>
								<?php endif; ?>
								<?php if ( 'on' === et_get_option( 'divi_show_twitter_icon', 'on' ) ) : ?>
									<li class="et-social-icon et-social-twitter">
										<a href="<?php echo esc_url( et_get_option( 'divi_twitter_url', '#' ) ); ?>" class="icon">
											<span><?php esc_html_e( 'Twitter', 'Divi' ); ?></span>
										</a>
									</li>
								<?php endif; ?>
									<li class="et-social-icon et-social-instagram">
										<a href="https://www.instagram.com/tramlines" class="icon">
											<span><?php esc_html_e( 'Instagram', 'Divi' ); ?></span>
										</a>
									</li>
								<?php if ( 'on' === et_get_option( 'divi_show_rss_icon', 'on' ) ) : ?>
								<?php
									$et_rss_url = '' !== et_get_option( 'divi_rss_url' )
										? et_get_option( 'divi_rss_url' )
										: get_bloginfo( 'rss2_url' );
								?>
									<li class="et-social-icon et-social-rss">
										<a href="<?php echo esc_url( $et_rss_url ); ?>" class="icon">
											<span><?php esc_html_e( 'RSS', 'Divi' ); ?></span>
										</a>
									</li>
								<?php endif; ?>

								</ul>




						<?php	}
						?>

						<p class="footer_legal">
							<!-- <a href="http://www.tramlines.org.uk/2018/privacy-policy/">Privacy Policy</a> -
							<a href="http://www.tramlines.org.uk/2018/terms-of-use/">Terms of use</a> - -->
							Copyright Tramlines Enterprises Ltd <?=date('Y');?>
						</p>

					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>

	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '861257404014913');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=861257404014913&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->


</body>
</html>
